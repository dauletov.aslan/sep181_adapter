package kz.astana.adapterapplication;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ListView listView = findViewById(R.id.listView);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                MainActivity.this,
                android.R.layout.simple_dropdown_item_1line,
                new String[]{"KZ", "RU", "US"}
        );
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        ArrayAdapter a = ArrayAdapter.createFromResource(
                MainActivity.this,
                R.array.countries,
                android.R.layout.simple_list_item_1
        );

        listView.setAdapter(a);

        Spinner spinner = findViewById(R.id.spinner);
//        spinner.setAdapter(adapter);

        CustomAdapter customAdapter = new CustomAdapter(
                MainActivity.this,
                getItems()
        );
        spinner.setAdapter(customAdapter);
        int spinnerItemCount = customAdapter.getCount();

        AutoCompleteTextView actv = findViewById(R.id.autoTextView);
        actv.setThreshold(1);

        ArrayAdapter<String> autoAdapter = new ArrayAdapter<String>(
                MainActivity.this,
                android.R.layout.simple_dropdown_item_1line,
                getCountriesName()
        );

        actv.setAdapter(autoAdapter);

        MultiAutoCompleteTextView multi = findViewById(R.id.multiTextView);
        multi.setThreshold(1);

        ArrayAdapter<String> multiAdapter = new ArrayAdapter<String>(
                MainActivity.this,
                android.R.layout.simple_list_item_1,
                getMulti()
        );
        multi.setAdapter(multiAdapter);
        multi.setTokenizer(new MultiAutoCompleteTextView.CommaTokenizer());

        Button button = findViewById(R.id.newActivity);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainActivity.this, ListViewActivity.class));
            }
        });
    }

    private String[] getCountriesName() {
        return new String[]{
                "Kazakhstan",
                "Russia",
                "China",
                "Japan",
                "Kyrgyzstan",
                "Iran",
                "Iraq"
        };
    }

    private String[] getMulti() {
        return new String[]{
                "Ukraine, Kiev",
                "USA, Washington",
                "France, Parish",
                "German, Berlin",
                "Australia, Sidney"
        };
    }

    private ArrayList<Item> getItems() {
        ArrayList<Item> items = new ArrayList<Item>();
        items.add(new Item("Aslan", 1));
        items.add(new Item("Ruslan", 10));
        items.add(new Item("Alisher", 12));
        items.add(new Item("Serik", 21));
        items.add(new Item("Karina", 41));
        items.add(new Item("Malika", 50));
        items.add(new Item("Aslan", 3));
        items.add(new Item("Naurysbay", 9));
        items.add(new Item("Nurbek", 11));
        items.add(new Item("Rustam", 12));

        return items;
    }
}