package kz.astana.adapterapplication;

public class Item {
    String title;
    int count;

    public Item(String title, int count) {
        this.title = title;
        this.count = count;
    }
}
