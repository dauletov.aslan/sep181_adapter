package kz.astana.adapterapplication;

import android.os.Bundle;
import android.widget.ListView;

import java.util.ArrayList;

import androidx.appcompat.app.AppCompatActivity;

public class ListViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);

        ListView listView = findViewById(R.id.listView);

        CustomAdapter adapter = new CustomAdapter(
                ListViewActivity.this,
                getItems()
        );

        listView.setAdapter(adapter);
    }

    private ArrayList<Item> getItems() {
        ArrayList<Item> items = new ArrayList<Item>();
        items.add(new Item("A", 1));
        items.add(new Item("B", 10));
        items.add(new Item("C", 12));
        items.add(new Item("D", 21));
        items.add(new Item("E", 41));
        items.add(new Item("F", 50));
        items.add(new Item("G", 3));
        items.add(new Item("H", 9));
        items.add(new Item("I", 11));
        items.add(new Item("J", 12));

        return items;
    }
}